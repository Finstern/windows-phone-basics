﻿using System.Linq;
using System.Media;
using System;

namespace WindowsPhoneBasics
{
	/// <summary>
	/// The flip Tile template flips from front to back.
	/// @image html http://i.msdn.microsoft.com/dynimg/IC625724.png
	/// 
	/// Flip Tile Properties:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC625725.png
	/// 
	/// Flip Tile Sizing Info:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC635949.png
	/// 
	/// Read http://msdn.microsoft.com/en-us/library/windowsphone/design/jj662924(v=vs.105).aspx for more information
	/// </summary>
	public class WP8IconicTile
	{
		/// <summary>
		/// !WARNING! Your app must be set to use Iconic Tiles for these settings to work. If you are using Flip or Cycle tiles, your app will crash!
		/// By default Unity apps are set to Flip Tile.
		/// To change this setting open the "WMAppManifest.xml" file in your built out solution.
		/// 
		/// This will update the primary iconic tile. The user must have elected to pin your app to the front page, it cannot be added programatically.
		/// </summary>
		/// <param name="title">A short message about the game and/or game title</param>
		/// <param name="count">This can be anything, from how many plants you need to water, to how many times your character has jumped or even a highscore</param>
		/// <param name="wideContent1">A small body of text displayed on the wide Icon</param>
		/// <param name="wideContent2">A small body of text displayed on the wide Icon, under the above</param>
		/// <param name="wideContent3">A small body of text displayed on the wide Icon, under the above</param>
		/// <param name="smallIconImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="iconImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="red">A number between 0 and 255</param>
		/// <param name="green">A number between 0 and 255</param>
		/// <param name="blue">A number between 0 and 255</param>
		public static void UpdatePrimaryIconicTile(string title, int count, string wideContent1, string wideContent2, string wideContent3, Uri smallIconImage, Uri iconImage, byte red, byte green, byte blue)
		{
			
		}

		/// <summary>
		/// This will reset the primary flip tile to what it was when the game was first installed.
		/// </summary>
		public static void ClearPrimaryIconicTile()
		{

		}

	}
}

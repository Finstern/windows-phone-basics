﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace WindowsPhoneBasics
{
	/// <summary>
	/// Contains all the functions to access the integrated Windows Phone 8 Choosers and Launchers.
	/// </summary>
    public class WP8Tasks
    {
		
		///<summary>
		///This will auto enter the status to be shared. It is editable by the user.
		/// </summary>
		/// <param name="message">User editable, good for fan promotion.</param>
		public static void ShareStatus(string message)
		{
			
		}

		/// <summary>
		/// ///This will auto enter the link to be shared. Best used for player scores and progress as the Title cannot be edited.
		/// </summary>
		/// <param name="linkTitle">Not editable by the user. "I got a score of 52 in *Game Name*"</param>
		/// <param name="url">Link to your website and/or where the app is available from.</param>
		/// <param name="message">Editable by the player. Good for bragging rights.</param>
		public static void ShareLink(string linkTitle, string url, string message)
		{

		}

		/// <summary>
		/// You must give a valid path to an image. Unfortunately Windows Phone prevents apps from access images anywhere except the media gallery.
		/// </summary>
		/// <param name="filePath">Valid path to file</param>
		public static void ShareMedia(string filePath)
		{

		}

		/// <summary>
		/// Will open the SMS Composer and and will fill in any information you wish.
		/// Can be edited by the user.
		/// </summary>
		/// <param name="phoneNumber">Can be left blank. A good use would to fill in your companies Support phone number.</param>
		/// <param name="smsBody">User editable, can be autofilled. If used for support could include error codes etc</param>
		public static void ComposeSMS(string phoneNumber, string smsBody)
		{

		}

		/*/// <summary>
		/// The phone call task launches the Phone application and displays the phone number and display name that you specify.
		/// The phone call is not placed until the user presses the call button.
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="displayName"></param>
		public static void PhoneCall(string phoneNumber, string displayName)
		{

		}
		*/

		/// <summary>
		/// Will open up and compose an Email. Good for support, can be used to get a user to submit a bug report immediately.
		/// </summary>
		/// <param name="subject">Email Subject. Can auto fill with something like [Bug Report] Error Code</param>
		/// <param name="body">Email Body. Again, can be filled with a crash report and information about game state.</param>
		/// <param name="recipientAddress">Can be left blank or set to anything. eg support.yourcompany.com</param>
		public static void ComposeEmail(string subject, string body, string recipientAddress)
		{

		}

		/// <summary>
		/// Will open up and compose an Email.
		/// </summary>
		/// <param name="subject">Email Subject. Can auto fill with something like [Bug Report] Error Code</param>
		/// <param name="body">Email Body. Again, can be filled with a crash report and information about game state.</param>
		/// <param name="recipientAddress">Can be left blank or set to anything. eg support.yourcompany.com</param>
		/// <param name="ccAddress">Send to multiple members of the team.</param>
		public static void ComposeEmail(string subject, string body, string recipientAddress, string ccAddress)
		{

		}

		/// <summary>
		/// Will open up and compose an Email.
		/// </summary>
		/// <param name="subject">Email Subject. Can auto fill with something like [Bug Report] Error Code</param>
		/// <param name="body">Email Body. Again, can be filled with a crash report and information about game state.</param>
		/// <param name="recipientAddress">Can be left blank or set to anything. eg support.yourcompany.com</param>
		/// <param name="ccAddress">Send to multiple members of the team.</param>
		/// <param name="bccAddress">Send to multiple people, but hide addresses from recipients.</param>
		public static void ComposeEmail(string subject, string body, string recipientAddress, string ccAddress, string bccAddress)
		{

		}

		/// <summary>
		/// Can be used to open up any web address in the browser.
		/// </summary>
		/// <param name="url">The address of the website. MUST have http://www. in front of any address.</param>
		public static void OpenWebBrowser(string url)
		{

		}

		/// <summary>
		/// This should bring you to the games review/ratings page.
		/// </summary>
		public static void AskPlayerToRate()
		{
			
		}

		/// <summary>
		/// Brings the user directly to an app page in Marketplace. Useful for suggesting other games you have made.
		/// </summary>
		/// <param name="appContentIdentifier">eg. "c14e93aa-27d7-df11-a844-00237de2db9e"</param>
		public static void GoToAppInMarketPlace(string appContentIdentifier)
		{

		}

		/// <summary>
		/// Let the user search for something in Marketplace.
		/// </summary>
		/// <param name="searchTerms">eg. "Your companies name"</param>
		public static void SearchMarketPlace(string searchTerms)
		{

		}

		/// <summary>
		/// Asks the user if they'd like to add an event into their calandar.
		/// Less hostile than forcing notifications on them.
		/// </summary>
		/// <param name="startTime">DateTime, the start time of the notification. Use DateTime.Now.AddHours(2) etc</param>
		/// <param name="endTime">DateTime, the end time of the notification. Use DateTime.Now.AddHours(3) etc Usually more hours than the start time</param>
		/// <param name="appointmentSubject">Can be something like "Remember to water plants!"</param>
		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject)
		{ 
		
		}

		/// <summary>
		/// Asks the user if they'd like to add an event into their calandar.
		/// Less hostile than forcing notifications on them.
		/// </summary>
		/// <param name="startTime">DateTime, the start time of the notification. Use DateTime.Now.AddHours(2) etc</param>
		/// <param name="endTime">DateTime, the end time of the notification. Use DateTime.Now.AddHours(3) etc Usually more hours than the start time</param>
		/// <param name="appointmentSubject">Can be something like "Remember to water plants!"</param>
		/// <param name="appointmentLocation">Name of the game? Area in game?</param>
		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation)
		{

		}

		/// <summary>
		/// Asks the user if they'd like to add an event into their calandar.
		/// Less hostile than forcing notifications on them.
		/// </summary>
		/// <param name="startTime">DateTime, the start time of the notification. Use DateTime.Now.AddHours(2) etc</param>
		/// <param name="endTime">DateTime, the end time of the notification. Use DateTime.Now.AddHours(3) etc Usually more hours than the start time</param>
		/// <param name="appointmentSubject">Can be something like "Remember to water plants!"</param>
		/// <param name="appointmentLocation"> Name of the game? Area in game?</param>
		/// <param name="details">If its a complicated procedure, can detail it here, or offer suggestions on what to do once thing is done.</param>
		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation, string details)
		{

		}

		/// <summary>
		/// Asks the user if they'd like to add an event into their calandar.
		/// Less hostile than forcing notifications on them.
		/// </summary>
		/// <param name="startTime">DateTime, the start time of the notification. Use DateTime.Now.AddHours(2) etc</param>
		/// <param name="endTime">DateTime, the end time of the notification. Use DateTime.Now.AddHours(3) etc Usually more hours than the start time</param>
		/// <param name="appointmentSubject">Can be something like "Remember to water plants!"</param>
		/// <param name="appointmentLocation">Name of the game? Area in game?</param>
		/// <param name="details">If its a complicated procedure, can detail it here, or offer suggestions on what to do once thing is done.</param>
		/// <param name="isAllDayEvent">true or false</param>
		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation, string details, bool isAllDayEvent)
		{ 
		
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPhoneBasics
{
	/// <summary>
	/// This allows you to create a new Secondary Tile. This tile can be used to display different information from the Main Tile, it can also change tile types from Standard to Flip, Cycle or Iconic
	/// Only one Secondary tile can exist and once created the user will be guided to it immediately. It can be unpinned any time by the user.
	/// </summary>
	public class WP8SecondaryTile
	{
		/// <summary>
		/// This will create a Secondary "Standard" Tile. A standard tile is the most basic of tiles. It can be upgraded to a Flip, Cycle or Iconic tile at any time.
		/// </summary>
		/// <param name="title">A short message about the game and/or game title</param>
		/// <param name="backTitle">Something to describe the information on the back of the Icon</param>
		/// <param name="backContent">A small body of text displayed on the back of the square Icon</param>
		/// <param name="wideBackContent">A larger body of text displayed on the back of the wide Icon</param>
		/// <param name="count">This can be anything, from how many plants you need to water, to how many times your character has jumped or even a highscore</param>
		/// <param name="backroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="backBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		public static void CreateSecondaryTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri backroundImage, Uri backBackgroundImage)
		{

		}

		/// <summary>
		/// This will change the existing Secondary Tile.
		/// </summary>
		/// <param name="title">A short message about the game and/or game title</param>
		/// <param name="backTitle">Something to describe the information on the back of the Icon</param>
		/// <param name="backContent">A small body of text displayed on the back of the square Icon</param>
		/// <param name="wideBackContent">A larger body of text displayed on the back of the wide Icon</param>
		/// <param name="count">This can be anything, from how many plants you need to water, to how many times your character has jumped or even a highscore</param>
		/// <param name="backroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="backBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		public static void UpdateSecondaryTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri backroundImage, Uri backBackgroundImage)
		{

		}

		/// <summary>
		/// This will delete the Secondary Tile.
		/// </summary>
		public static void DeleteSecondaryTile()
		{

		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPhoneBasics
{
	/// <summary>
	/// Contains all the Functions to access the Windows Phone 8 Hardware.
	/// </summary>
	public class WP8HW
	{
		/// <summary>
		/// Will vibrate the for x milliseconds
		/// </summary>
		/// <param name="millisecondsToVibrate"></param>
		public static void VibratePhone(double millisecondsToVibrate)
		{
			
		}

		/// <summary>
		/// Will stop the phone vibrating
		/// </summary>
		public static void StopVibration()
		{

		}
	}
}

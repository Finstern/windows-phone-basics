﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPhoneBasics
{
	/// <summary>
	/// The flip Tile template flips from front to back.
	/// @image html http://i.msdn.microsoft.com/dynimg/IC601177.png
	/// 
	/// Flip Tile Properties:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC601179.png
	/// 
	/// Flip Tile Sizing Info:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC619088.png
	/// 
	/// Read http://msdn.microsoft.com/en-us/library/windowsphone/develop/jj662923(v=vs.105).aspx for more information
	/// </summary>
	public class WP8CycleTile
	{
		/// <summary>
		/// !WARNING! Your app must be set to use Cycle Tiles for these settings to work. If you are using Iconic or Flip tiles, your app will crash!
		/// By default Unity apps are set to Flip Tile.
		/// To change this setting open the "WMAppManifest.xml" file in your built out solution.
		/// 
		/// This will update the primary Cycle Tile. The user must have elected to pin your app to the front page, it cannot be added programatically.
		/// </summary>
		/// <param name="title">A short message about the game and/or game title</param>
		/// <param name="count">This can be anything, from how many plants you need to water, to how many times your character has jumped or in this case, the number of images in the Cycle</param>
		/// <param name="smallBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="imageArray">An array of Uri's. There must be at least 1 and at most 9. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		public static void UpdatePrimaryCycleTile(string title, int count, Uri smallBackgroundImage, Uri[] imageArray)
		{

		}

		/// <summary>
		/// This will reset the primary flip tile to what it was when the game was first installed.
		/// </summary>
		public static void ClearPrimaryCycleTile()
		{

		}
	}
}

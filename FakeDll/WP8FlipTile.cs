﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPhoneBasics
{
	/// <summary>
	/// The flip Tile template flips from front to back.
	/// @image html http://i.msdn.microsoft.com/dynimg/IC600901.png
	/// 
	/// Flip Tile Properties:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC600902.png
	/// 
	/// Flip Tile Sizing Info:
	/// @image html http://i.msdn.microsoft.com/dynimg/IC619133.png
	/// 
	/// Read http://msdn.microsoft.com/en-us/library/windowsphone/design/jj662925(v=vs.105).aspx for more information
	/// </summary>
	public class WP8FlipTile
	{
		/// <summary>
		/// !WARNING! Your app must be set to use Flip Tiles for these settings to work. If you are using Iconic or Cycle tiles, your app will crash!
		/// By default Unity apps are set to Flip Tile.
		/// To change this setting open the "WMAppManifest.xml" file in your built out solution.
		/// 
		/// This will update the primary Flip Tile. The user must have elected to pin your app to the front page, it cannot be added programatically.
		/// </summary>
		/// <param name="title">A short message about the game and/or game title</param>
		/// <param name="backTitle">Something to describe the information on the back of the Icon</param>
		/// <param name="backContent">A small body of text displayed on the back of the square Icon</param>
		/// <param name="wideBackContent">A larger body of text displayed on the back of the wide Icon</param>
		/// <param name="count">This can be anything, from how many plants you need to water, to how many times your character has jumped or even a highscore</param>
		/// <param name="smallBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="backroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="backBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="wideBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		/// <param name="wideBackBackgroundImage">See sizing information above. Must be either in relative path format "/Assets/Tiles/FlipTiles/image.png" or Absolute path eg "http://www.google.com/image.png"</param>
		public static void UpdatePrimaryFlipTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri smallBackgroundImage, Uri backroundImage, Uri backBackgroundImage, Uri wideBackgroundImage, Uri wideBackBackgroundImage)
		{

		}

		/// <summary>
		/// This will reset the primary flip tile to what it was when the game was first installed.
		/// </summary>
		public static void ClearPrimaryFlipTile()
		{

		}
	}
}

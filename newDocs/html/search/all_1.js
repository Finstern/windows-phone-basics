var searchData=
[
  ['clearprimarycycletile',['ClearPrimaryCycleTile',['../class_windows_phone_basics_1_1_w_p8_cycle_tile.html#af7a15786687d036017fc4f55f3b4fc2f',1,'WindowsPhoneBasics::WP8CycleTile']]],
  ['clearprimaryfliptile',['ClearPrimaryFlipTile',['../class_windows_phone_basics_1_1_w_p8_flip_tile.html#a048e8f16c0ac6742772d68a54f95bb64',1,'WindowsPhoneBasics::WP8FlipTile']]],
  ['clearprimaryiconictile',['ClearPrimaryIconicTile',['../class_windows_phone_basics_1_1_w_p8_iconic_tile.html#a99aefb7ebb28a9e7e861fc58895b4c2e',1,'WindowsPhoneBasics::WP8IconicTile']]],
  ['composeemail',['ComposeEmail',['../class_windows_phone_basics_1_1_w_p8_tasks.html#a9bef73834727908d4277c905f11463b5',1,'WindowsPhoneBasics.WP8Tasks.ComposeEmail(string subject, string body, string recipientAddress)'],['../class_windows_phone_basics_1_1_w_p8_tasks.html#a233486f4f09dbe7d9c8b46ecffa81075',1,'WindowsPhoneBasics.WP8Tasks.ComposeEmail(string subject, string body, string recipientAddress, string ccAddress)'],['../class_windows_phone_basics_1_1_w_p8_tasks.html#ae055e32dd04f6457bbac024038700b1d',1,'WindowsPhoneBasics.WP8Tasks.ComposeEmail(string subject, string body, string recipientAddress, string ccAddress, string bccAddress)']]],
  ['composesms',['ComposeSMS',['../class_windows_phone_basics_1_1_w_p8_tasks.html#a62663e0c902ff5e78bc4d1c04375c866',1,'WindowsPhoneBasics::WP8Tasks']]],
  ['createsecondarytile',['CreateSecondaryTile',['../class_windows_phone_basics_1_1_w_p8_secondary_tile.html#ab36b111e0ededddb1cd5b69f4052eebd',1,'WindowsPhoneBasics::WP8SecondaryTile']]]
];

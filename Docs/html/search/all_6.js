var searchData=
[
  ['updateprimarycycletile',['UpdatePrimaryCycleTile',['../class_windows_phone_basics_1_1_w_p8_cycle_tile.html#ae8b2daf0726f77ce561f46c335497fc7',1,'WindowsPhoneBasics::WP8CycleTile']]],
  ['updateprimaryfliptile',['UpdatePrimaryFlipTile',['../class_windows_phone_basics_1_1_w_p8_flip_tile.html#ab26d28d07e772d04484edf1d16300b28',1,'WindowsPhoneBasics::WP8FlipTile']]],
  ['updateprimaryiconictile',['UpdatePrimaryIconicTile',['../class_windows_phone_basics_1_1_w_p8_iconic_tile.html#a1dee6e1dee438a42de58fe74c3297f2f',1,'WindowsPhoneBasics::WP8IconicTile']]],
  ['updatesecondarytiletocycle',['UpdateSecondaryTileToCycle',['../class_windows_phone_basics_1_1_w_p8_secondary_tile.html#a4102c7921b072a264321470ec2124d55',1,'WindowsPhoneBasics::WP8SecondaryTile']]],
  ['updatesecondarytiletoflip',['UpdateSecondaryTileToFlip',['../class_windows_phone_basics_1_1_w_p8_secondary_tile.html#a543d396cce569bfdb6788b1342977454',1,'WindowsPhoneBasics::WP8SecondaryTile']]],
  ['updatesecondarytiletoiconic',['UpdateSecondaryTileToIconic',['../class_windows_phone_basics_1_1_w_p8_secondary_tile.html#a1f510d4f3223af9e2bd958ed065523a1',1,'WindowsPhoneBasics::WP8SecondaryTile']]]
];

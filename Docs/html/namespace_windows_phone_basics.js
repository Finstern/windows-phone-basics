var namespace_windows_phone_basics =
[
    [ "WP8CycleTile", "class_windows_phone_basics_1_1_w_p8_cycle_tile.html", null ],
    [ "WP8FlipTile", "class_windows_phone_basics_1_1_w_p8_flip_tile.html", null ],
    [ "WP8HW", "class_windows_phone_basics_1_1_w_p8_h_w.html", null ],
    [ "WP8IconicTile", "class_windows_phone_basics_1_1_w_p8_iconic_tile.html", null ],
    [ "WP8SecondaryTile", "class_windows_phone_basics_1_1_w_p8_secondary_tile.html", null ],
    [ "WP8Tasks", "class_windows_phone_basics_1_1_w_p8_tasks.html", null ]
];
﻿using System;
using Microsoft.Phone.Tasks;

namespace WindowsPhoneBasics
{
	public class WP8Tasks
	{
		public static void ShareStatus(string message)
		{
			ShareStatusTask shareStatusTask = new ShareStatusTask();
			shareStatusTask.Status = message;
			shareStatusTask.Show();
		}

		public static void ShareLink(string linkTitle, string url, string message)
		{
			ShareLinkTask shareLinkTask = new ShareLinkTask();
			shareLinkTask.Title = linkTitle;
			shareLinkTask.LinkUri = new Uri(url, UriKind.RelativeOrAbsolute);
			shareLinkTask.Message = message;
			shareLinkTask.Show();
		}

		public static void ShareMedia(string filePath)
		{
			ShareMediaTask shareMediaTask = new ShareMediaTask();
			shareMediaTask.FilePath = filePath;
			shareMediaTask.Show();
		}

		public static void ComposeSMS(string phoneNumber, string smsBody)
		{
			SmsComposeTask smsComposeTask = new SmsComposeTask();
			smsComposeTask.To = phoneNumber;
			smsComposeTask.Body = smsBody;
			smsComposeTask.Show();
		}

		/*public static void PhoneCall(string phoneNumber, string displayName)
		{
			PhoneCallTask phoneCallTask = new PhoneCallTask();
			phoneCallTask.PhoneNumber = phoneNumber;
			phoneCallTask.DisplayName = displayName;
			phoneCallTask.Show();
		}*/

		public static void ComposeEmail(string subject, string body, string recipientAddress)
		{
			EmailComposeTask emailComposeTask = new EmailComposeTask();
			emailComposeTask.Subject = subject;
			emailComposeTask.Body = body;
			emailComposeTask.To = recipientAddress;
			emailComposeTask.Show();
		}

		public static void ComposeEmail(string subject, string body, string recipientAddress, string ccAddress)
		{
			EmailComposeTask emailComposeTask = new EmailComposeTask();
			emailComposeTask.Subject = subject;
			emailComposeTask.Body = body;
			emailComposeTask.To = recipientAddress;
			emailComposeTask.Cc = ccAddress;
			emailComposeTask.Show();
		}

		public static void ComposeEmail(string subject, string body, string recipientAddress, string ccAddress, string bccAddress)
		{
			EmailComposeTask emailComposeTask = new EmailComposeTask();
			emailComposeTask.Subject = subject;
			emailComposeTask.Body = body;
			emailComposeTask.To = recipientAddress;
			emailComposeTask.Cc = ccAddress;
			emailComposeTask.Bcc = bccAddress;
			emailComposeTask.Show();
		}

		public static void OpenWebBrowser(string url)
		{
			WebBrowserTask webBrowserTask = new WebBrowserTask();
			webBrowserTask.Uri = new Uri(url, UriKind.RelativeOrAbsolute);
			webBrowserTask.Show();
		}

		public static void AskPlayerToRate()
		{
			MarketplaceReviewTask marketPlaceReviewTask = new MarketplaceReviewTask();
			marketPlaceReviewTask.Show();
		}

		public static void GoToAppInMarketPlace(string appContentIdentifier)
		{
			MarketplaceDetailTask marketplaceDetailTask = new MarketplaceDetailTask();
			marketplaceDetailTask.ContentIdentifier = appContentIdentifier;
			marketplaceDetailTask.ContentType = MarketplaceContentType.Applications;
			marketplaceDetailTask.Show();
		}

		public static void SearchMarketPlace(string searchTerms)
		{
			MarketplaceSearchTask marketplaceSearchTask = new MarketplaceSearchTask();
			marketplaceSearchTask.SearchTerms = searchTerms;
			marketplaceSearchTask.Show();
		}

		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject)
		{
			SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
			saveAppointmentTask.StartTime = DateTime.Now.AddHours(2);
			saveAppointmentTask.EndTime = DateTime.Now.AddHours(3);
			saveAppointmentTask.Subject = appointmentSubject;
			saveAppointmentTask.Location = " ";
			saveAppointmentTask.Details = " ";
			saveAppointmentTask.IsAllDayEvent = false;
			saveAppointmentTask.Reminder = Reminder.FifteenMinutes;
			saveAppointmentTask.AppointmentStatus = Microsoft.Phone.UserData.AppointmentStatus.Busy;
			saveAppointmentTask.Show();
		}

		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation)
		{
			SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
			saveAppointmentTask.StartTime = DateTime.Now.AddHours(2);
			saveAppointmentTask.EndTime = DateTime.Now.AddHours(3);
			saveAppointmentTask.Subject = appointmentSubject;
			saveAppointmentTask.Location = appointmentLocation;
			saveAppointmentTask.Details = " ";
			saveAppointmentTask.IsAllDayEvent = false;
			saveAppointmentTask.Reminder = Reminder.FifteenMinutes;
			saveAppointmentTask.AppointmentStatus = Microsoft.Phone.UserData.AppointmentStatus.Busy;
			saveAppointmentTask.Show();
		}

		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation, string details)
		{
			SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
			saveAppointmentTask.StartTime = DateTime.Now.AddHours(2);
			saveAppointmentTask.EndTime = DateTime.Now.AddHours(3);
			saveAppointmentTask.Subject = appointmentSubject;
			saveAppointmentTask.Location = appointmentLocation;
			saveAppointmentTask.Details = details;
			saveAppointmentTask.IsAllDayEvent = false;
			saveAppointmentTask.Reminder = Reminder.FifteenMinutes;
			saveAppointmentTask.AppointmentStatus = Microsoft.Phone.UserData.AppointmentStatus.Busy;
			saveAppointmentTask.Show();
		}

		public static void AddAppointmentInCalandar(DateTime startTime, DateTime endTime, string appointmentSubject, string appointmentLocation, string details, bool isAllDayEvent)
		{
			SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
			saveAppointmentTask.StartTime = DateTime.Now.AddHours(2);
			saveAppointmentTask.EndTime = DateTime.Now.AddHours(3);
			saveAppointmentTask.Subject = appointmentSubject;
			saveAppointmentTask.Location = appointmentLocation;
			saveAppointmentTask.Details = details;
			saveAppointmentTask.IsAllDayEvent = isAllDayEvent;
			saveAppointmentTask.Reminder = Reminder.FifteenMinutes;
			saveAppointmentTask.AppointmentStatus = Microsoft.Phone.UserData.AppointmentStatus.Busy;
			saveAppointmentTask.Show();
		}
	}


}

﻿using Microsoft.Devices.Sensors;
using System;
using Windows.Phone.Devices.Notification;

namespace WindowsPhoneBasics
{
	public class WP8HW
	{
		
		public static void VibratePhone(double millisecondsToVibrate)
		{
			VibrationDevice phoneVibrate = VibrationDevice.GetDefault();
			phoneVibrate.Vibrate(TimeSpan.FromMilliseconds(millisecondsToVibrate));
		}

		public static void StopVibration()
		{
			VibrationDevice phoneVibrate = VibrationDevice.GetDefault();
			phoneVibrate.Cancel();
		}
	}
}

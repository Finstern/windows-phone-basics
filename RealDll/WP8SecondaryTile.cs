﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WindowsPhoneBasics
{
	public class WP8SecondaryTile
	{
		public static void CreateSecondaryTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri backroundImage, Uri backBackgroundImage)
		{
			StandardTileData secondaryTileData = new StandardTileData()
			{
				Title = title,
				BackTitle = backTitle,
				BackContent = backContent,
				Count = count,
				BackgroundImage = backroundImage,
				BackBackgroundImage = backBackgroundImage,
			};
			ShellTile.Create(new Uri("/MainPage.xaml", UriKind.Relative), secondaryTileData);
		}

		public static void UpdateSecondaryTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri backroundImage, Uri backBackgroundImage)
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.FirstOrDefault(tile => tile.NavigationUri.ToString().Contains("/MainPage.xaml"));

			StandardTileData secondaryTileData = new StandardTileData()
			{
				Title = title,
				BackTitle = backTitle,
				BackContent = backContent,
				Count = count,
				BackgroundImage = backroundImage,
				BackBackgroundImage = backBackgroundImage,
			};
			pinnedTile.Update(secondaryTileData);
		}

		public static void DeleteSecondaryTile()
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.FirstOrDefault(tile => tile.NavigationUri.ToString().Contains("/MainPage.xaml"));

			pinnedTile.Delete();
		}
	}
}

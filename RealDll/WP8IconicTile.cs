﻿using Microsoft.Phone.Shell;
using System.Linq;
using System.Windows.Media;
using System;

namespace WindowsPhoneBasics
{
	public class WP8IconicTile
	{
		public static void UpdatePrimaryIconicTile(string title, int count, string wideContent1, string wideContent2, string wideContent3, Uri smallIconImage, Uri iconImage, byte red, byte green, byte blue)
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			IconicTileData tileData = new IconicTileData
			{
				Title = title,
				Count = count,
				WideContent1 = wideContent1,
				WideContent2 = wideContent2,
				WideContent3 = wideContent3,
				SmallIconImage = smallIconImage,
				IconImage = iconImage,
				BackgroundColor = new Color { A = 255, R = red, G = green, B = blue },
			};
			pinnedTile.Update(tileData);
		}

		public static void ClearPrimaryIconicTile()
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			IconicTileData tileData = new IconicTileData
			{
				Title = "",
				Count = 0,
				WideContent1 = "",
				WideContent2 = "",
				WideContent3 = "",
				SmallIconImage = new Uri("", UriKind.Relative),
				IconImage = new Uri("", UriKind.Relative),
				BackgroundColor = new Color {A=0,B=0,G=0,R=0},
			};
			pinnedTile.Update(tileData);
		}
	}
}

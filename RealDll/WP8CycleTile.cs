﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsPhoneBasics
{
	public class WP8CycleTile
	{
		public static void UpdatePrimaryCycleTile(string title, int count, Uri smallBackgroundImage, Uri[] imageArray)
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			CycleTileData tileData = new CycleTileData()
			{
				Title = title,
				Count = count,
				SmallBackgroundImage = smallBackgroundImage,
				CycleImages = imageArray,
			};

			pinnedTile.Update(tileData);
		}

		public static void ClearPrimaryCycleTile()
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			CycleTileData tileData = new CycleTileData()
			{
				Title = "",
				Count = 0,
				SmallBackgroundImage = new Uri("", UriKind.Relative),
				CycleImages = new Uri[]
				{
				new Uri("", UriKind.Relative),	
				},
			};

			pinnedTile.Update(tileData);
		}
	}
}

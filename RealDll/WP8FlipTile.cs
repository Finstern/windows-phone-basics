﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsPhoneBasics
{
	public class WP8FlipTile
	{
		public static void UpdatePrimaryFlipTile(string title, string backTitle, string backContent, string wideBackContent, int count, Uri smallBackgroundImage, Uri backroundImage, Uri backBackgroundImage, Uri wideBackgroundImage, Uri wideBackBackgroundImage)
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			FlipTileData tileData = new FlipTileData
			{
			   Title = title,
			   BackTitle = backTitle,
			   BackContent = backContent,
			   WideBackContent = wideBackContent,
			   Count = count,
			   SmallBackgroundImage = smallBackgroundImage,
			   BackgroundImage = backroundImage,
			   BackBackgroundImage = backBackgroundImage,
			   WideBackgroundImage = wideBackgroundImage,
			   WideBackBackgroundImage = wideBackBackgroundImage,
			};
			pinnedTile.Update(tileData);
		}

		public static void ClearPrimaryFlipTile()
		{
			ShellTile pinnedTile = ShellTile.ActiveTiles.First();

			FlipTileData tileData = new FlipTileData
			{
				Title = "",
				BackTitle = "",
				BackContent = "",
				WideBackContent = "",
				Count = 0,
				SmallBackgroundImage = new Uri("", UriKind.Relative),
				BackgroundImage = new Uri("", UriKind.Relative),
				BackBackgroundImage = new Uri("", UriKind.Relative),
				WideBackgroundImage = new Uri("", UriKind.Relative),
				WideBackBackgroundImage = new Uri("", UriKind.Relative),
			};
			pinnedTile.Update(tileData);
		}
	}
}

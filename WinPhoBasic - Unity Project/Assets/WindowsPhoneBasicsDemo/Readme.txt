Windows Phone Basics for Unity

Use any of the following with just a single line of code!

Social Features:
Share Status
Share Link
Share Media
Compose SMS
Compose Email
Open Webpage
Go To App In Marketplace
Search Marketplace
Add Calandar Appointment

Tile Features:
Update Main Flip/Cycle/Iconic Tiles
Create New Secondary Tiles
Update Secondary Tiles
Change Secondary Tiles to Flip/Cycle/Iconic Tiles on the fly!

Hardware Features:
Phone Vibration

Please email plugins@shaneobrien.ie if you have any suggestions, I will try my best to help!

For extensive documentation please visit: http://www.shaneobrien.ie/prog/plugins/UnityWindowsPhoneBasics/index.html